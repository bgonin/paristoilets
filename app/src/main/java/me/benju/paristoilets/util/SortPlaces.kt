package me.benju.paristoilets.util

import com.google.android.gms.maps.model.LatLng
import me.benju.paristoilets.model.Place

class SortPlaces(private var currentLoc: LatLng) : Comparator<Place> {

    override fun compare(place1: Place, place2: Place): Int {
        val lat1 = place1.fields.geom_x_y[0]
        val lon1 = place1.fields.geom_x_y[1]
        val lat2 = place2.fields.geom_x_y[0]
        val lon2 = place2.fields.geom_x_y[1]

        val distanceToPlace1 = distance(currentLoc.latitude, currentLoc.longitude, lat1, lon1)
        place1.distance = distanceToPlace1
        val distanceToPlace2 = distance(currentLoc.latitude, currentLoc.longitude, lat2, lon2)
        place2.distance = distanceToPlace2
        return (distanceToPlace1 - distanceToPlace2).toInt()
    }

    fun distance(fromLat: Double, fromLon: Double, toLat: Double, toLon: Double): Double {
        val radius = 6378137.0   // approximate Earth radius, *in meters*

        // great circle distance formula and convert in meters
        val deltaLat = Math.toRadians(toLat - fromLat)
        val deltaLon = Math.toRadians(toLon - fromLon)
        val lat1 = Math.toRadians(fromLat)
        val lat2 = Math.toRadians(toLat)
        val aVal = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2) + Math.sin(deltaLon / 2) * Math.sin(deltaLon / 2) * Math.cos(lat1) * Math.cos(lat2)
        val cVal = 2 * Math.atan2(Math.sqrt(aVal), Math.sqrt(1 - aVal))

        return radius * cVal
    }

}