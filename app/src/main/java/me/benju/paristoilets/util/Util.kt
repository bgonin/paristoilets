package me.benju.paristoilets.util

import android.content.Context
import android.content.Intent
import me.benju.paristoilets.TOILET_DETAIL_EXTRA
import me.benju.paristoilets.model.Place
import me.benju.paristoilets.ui.detail.ToiletDetailActivity

fun getCompleteAddress(record: Place): String {
    var title = "${record.fields.numero_voie} ${record.fields.nom_voie}"
    if (record.fields.numero_voie == null) {
        title = record.fields.nom_voie
    }
    if (record.fields.arrondissement != null) {
        title += "\n750${record.fields.arrondissement} Paris, France"
    }
    return title
}

fun openToiletDetail(context: Context, record: Place) {
    val toiletDetailIntent = Intent(context, ToiletDetailActivity::class.java)
    toiletDetailIntent.putExtra(TOILET_DETAIL_EXTRA, record)
    context.startActivity(toiletDetailIntent)
}