package me.benju.paristoilets.util

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Typeface
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.Marker
import me.benju.paristoilets.R

fun changeMapStyle(context: Context, googleMap: GoogleMap) {
    try {
        // Customise the styling of the base map using a JSON object defined in a raw resource file.
        val success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.style_json))
        if (!success) {
            Log.e("Map", "Style parsing failed.")
        }
    } catch (e: Resources.NotFoundException) {
        Log.e("Map", "Can't find style. Error: ", e)
    }
}

fun setCustomInfoWindow(context: Context, map: GoogleMap) {
    map.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
        override fun getInfoWindow(arg0: Marker): View? {
            return null
        }

        override fun getInfoContents(marker: Marker): View {
            val info = LinearLayout(context)
            info.orientation = LinearLayout.VERTICAL

            val title = TextView(context)
            title.setTextColor(Color.BLACK)
            title.gravity = Gravity.CENTER
            title.setTypeface(null, Typeface.BOLD)
            title.text = marker.title

            val snippet = TextView(context)
            snippet.gravity = Gravity.CENTER
            snippet.setTextColor(Color.GRAY)
            snippet.text = marker.snippet

            info.addView(title)
            info.addView(snippet)

            return info
        }
    })
}