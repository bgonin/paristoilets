package me.benju.paristoilets.api

import io.reactivex.Observable
import me.benju.paristoilets.model.Records
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    // Returns a list of toilets, located in Paris and in Ile de France.
    @GET("api/records/1.0/search/")
    fun getRecords(@Query("dataset") dataset: String,
                   @Query("rows") rows: Int): Observable<Records>

}