package me.benju.paristoilets

const val baseUrl = "https://data.ratp.fr/"

const val dataSet = "sanisettesparis2011"
const val rows = 1000

const val centerOfParisLat = 48.856213
const val centerOfParisLon = 2.352201

const val DEFAULT_ZOOM = 12.0f

const val TOILET_DETAIL_EXTRA = "toilet_detail"
const val LIST_EXTRA = "list"