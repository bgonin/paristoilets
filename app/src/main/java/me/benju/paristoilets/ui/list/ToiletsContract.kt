package me.benju.paristoilets.ui.list

import me.benju.paristoilets.model.Place
import me.benju.paristoilets.ui.BasePresenter
import me.benju.paristoilets.ui.BaseView
import java.util.*

interface ToiletsContract {

    interface View : BaseView<Presenter> {
        fun setToiletsList(list: ArrayList<Place>)
    }

    interface Presenter : BasePresenter {
        fun getToiletsList()
    }

}