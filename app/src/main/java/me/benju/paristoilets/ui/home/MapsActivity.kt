package me.benju.paristoilets.ui.home

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.orhanobut.hawk.Hawk
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.benju.paristoilets.*
import me.benju.paristoilets.api.ToiletsService
import me.benju.paristoilets.model.Place
import me.benju.paristoilets.ui.list.ToiletsListActivity
import me.benju.paristoilets.util.*
import java.util.*

//TODO MVP
class MapsActivity : AppCompatActivity(),
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnInfoWindowClickListener {

    private var mMap: GoogleMap? = null

    private var mPermissionDenied = false

    private val parisLocation = LatLng(centerOfParisLat, centerOfParisLon)

    private val service = ToiletsService()
    private var toiletsList = arrayListOf<Place>()

    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private var mLastLocation: LatLng = parisLocation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.list -> {
                val listIntent = Intent(this, ToiletsListActivity::class.java)
                listIntent.putParcelableArrayListExtra(LIST_EXTRA, toiletsList)
                startActivity(listIntent)
                true
            }
            R.id.refresh -> {
                refresh()
                Toast.makeText(this, "Refresh data", Toast.LENGTH_SHORT).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        changeMapStyle(this, googleMap)
        setCustomInfoWindow(applicationContext, mMap!!)

        // Move and zoom to Paris
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(mLastLocation))
        mMap!!.moveCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM))

        getParisToiletsData()

        mMap!!.setOnInfoWindowClickListener(this)
        mMap!!.setOnMyLocationButtonClickListener(this)
        mMap!!.setOnMyLocationClickListener(this)
        enableMyLocation()
        mMap!!.uiSettings.isCompassEnabled = true
        mMap!!.uiSettings.isZoomControlsEnabled = true
    }

    private fun getParisToiletsData() {
        if (Hawk.contains(LIST_EXTRA)) {
            toiletsList = Hawk.get(LIST_EXTRA)
            displayMarkers()
        } else {
            refresh()
        }
    }

    private fun refresh() {
        service.api.getRecords(dataSet, rows)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    toiletsList = result.records as ArrayList<Place>
                    Hawk.put(LIST_EXTRA, toiletsList)
                    displayMarkers()
                }, { error ->
                    error.printStackTrace()
                    showErrorMessage()
                })
    }

    private fun showErrorMessage() {
        Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show()
    }

    // Loop all the records and add a marker
    private fun displayMarkers() {
        for (record in toiletsList) {
            val position = LatLng(record.fields.geom_x_y[0], record.fields.geom_x_y[1])
            val title = getCompleteAddress(record)
            val info = "${getString(R.string.times)} ${record.fields.horaires_ouverture}\n${getString(R.string.more_info)}"

            val marker = mMap!!.addMarker(MarkerOptions()
                    .position(position)
                    .title(title)
                    .snippet(info)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_wc_red_500_36dp)))
            marker.tag = record
        }
    }

    override fun onInfoWindowClick(marker: Marker?) {
        val record = marker?.tag as Place
        openToiletDetail(this, record)
    }

    //region USER LOCATION
    // Enables the My Location layer if the fine location permission has been granted.
    private fun enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_PERMISSION_REQUEST_CODE)
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap!!.isMyLocationEnabled = true
            mFusedLocationClient.lastLocation.addOnSuccessListener { location ->
                if (location != null) {
                    Log.i("location", location.toString())
                    mLastLocation = LatLng(location.latitude, location.longitude)
                    Collections.sort(toiletsList, SortPlaces(mLastLocation))

                    // Move the camera to your location
                    mMap!!.moveCamera(CameraUpdateFactory.newLatLng(mLastLocation))
                }
            }
        }
    }

    override fun onMyLocationButtonClick(): Boolean {
        return false
    }

    override fun onMyLocationClick(location: Location) {
        Toast.makeText(this, "Current location: " + location.latitude + ", " + location.longitude, Toast.LENGTH_LONG).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return
        }

        mPermissionDenied = true
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPermissionDenied = false
                    enableMyLocation()
                }
            }
        }
    }
    //endregion

    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

}