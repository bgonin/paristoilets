package me.benju.paristoilets.ui.list

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import me.benju.paristoilets.R
import me.benju.paristoilets.model.Place
import me.benju.paristoilets.util.bind
import java.util.*

class ToiletsListActivity : AppCompatActivity(), ToiletsContract.View {

    private val recyclerView by bind<RecyclerView>(R.id.toilets_list)

    private lateinit var presenter: ToiletsContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_toilets_list)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        ToiletsPresenter(this, intent)
        presenter.start()
    }

    override fun setPresenter(presenter: ToiletsContract.Presenter) {
        this.presenter = presenter
    }

    override fun setToiletsList(list: ArrayList<Place>) {
        recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL))
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.setHasFixedSize(true)

        val adapter = ToiletsAdapter(this, list)
        recyclerView.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

}