package me.benju.paristoilets.ui.list

import android.content.Intent
import me.benju.paristoilets.LIST_EXTRA
import me.benju.paristoilets.model.Place

class ToiletsPresenter(
        val view: ToiletsContract.View,
        val intent: Intent
) : ToiletsContract.Presenter {

    init {
        this.view.setPresenter(this)
    }

    override fun start() {
        getToiletsList()
    }

    override fun getToiletsList() {
        val toiletsList = intent.getParcelableArrayListExtra<Place>(LIST_EXTRA)
        view.setToiletsList(toiletsList)
    }

}