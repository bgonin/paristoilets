package me.benju.paristoilets.ui.detail

import android.content.Intent
import me.benju.paristoilets.TOILET_DETAIL_EXTRA
import me.benju.paristoilets.model.Place

class ToiletDetailPresenter(
        val view: ToiletDetailContract.View,
        val intent: Intent
) : ToiletDetailContract.Presenter {

    init {
        this.view.setPresenter(this)
    }

    override fun start() {
        getToiletDetail()
    }

    override fun getToiletDetail() {
        val detail = intent.getParcelableExtra<Place>(TOILET_DETAIL_EXTRA)
        view.setUi(detail)
    }

}