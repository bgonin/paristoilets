package me.benju.paristoilets.ui

interface BasePresenter {
    fun start()
}