package me.benju.paristoilets.ui.list

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import me.benju.paristoilets.R
import me.benju.paristoilets.model.Place
import me.benju.paristoilets.util.*

class ToiletsAdapter(private var context: Activity, private var toilets: List<Place>)
    : RecyclerView.Adapter<ToiletsAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val toilet = toilets[position]
        holder.address.text = "${String.format("%.2f", toilet.distance)} m - ${getCompleteAddress(toilet)}"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder = ViewHolder(parent.inflate(R.layout.item_toilet, false))
        return viewHolder.listen { position, _ ->
            openToiletDetail(context, toilets[position])
        }
    }

    override fun getItemCount(): Int = toilets.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val icon by itemView.bind<ImageView>(R.id.icon)
        val name by itemView.bind<TextView>(R.id.name)
        val address by itemView.bind<TextView>(R.id.address)
    }

}