package me.benju.paristoilets.ui

interface BaseView<T> {
    fun setPresenter(presenter: T)
}