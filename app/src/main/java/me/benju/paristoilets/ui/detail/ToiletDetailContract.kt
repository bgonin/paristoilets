package me.benju.paristoilets.ui.detail

import me.benju.paristoilets.model.Place
import me.benju.paristoilets.ui.BasePresenter
import me.benju.paristoilets.ui.BaseView

interface ToiletDetailContract {

    interface View : BaseView<Presenter> {
        fun setUi(toiletDetail: Place)
    }

    interface Presenter : BasePresenter {
        fun getToiletDetail()
    }

}