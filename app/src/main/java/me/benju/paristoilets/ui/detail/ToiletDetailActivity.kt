package me.benju.paristoilets.ui.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import me.benju.paristoilets.R
import me.benju.paristoilets.model.Place
import me.benju.paristoilets.util.bind
import me.benju.paristoilets.util.changeMapStyle
import me.benju.paristoilets.util.getCompleteAddress

class ToiletDetailActivity : AppCompatActivity(), OnMapReadyCallback, ToiletDetailContract.View {

    private val address by bind<TextView>(R.id.detail_address)
    private val openingTimes by bind<TextView>(R.id.detail_opening_times)
    private val administrator by bind<TextView>(R.id.detail_administrator)

    private lateinit var presenter: ToiletDetailContract.Presenter

    private lateinit var detail: Place

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_toilet_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        ToiletDetailPresenter(this, intent)
        presenter.start()

        val detailMapFragment = supportFragmentManager.findFragmentById(R.id.detail_map) as SupportMapFragment
        detailMapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        if (detail != null) {
            changeMapStyle(this, googleMap)

            val position = LatLng(detail.fields.geom_x_y[0], detail.fields.geom_x_y[1])
            googleMap.addMarker(MarkerOptions().position(position))
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(position))
        }
    }

    override fun setPresenter(presenter: ToiletDetailContract.Presenter) {
        this.presenter = presenter
    }

    // Set address, opening times and administrator
    override fun setUi(toiletDetail: Place) {
        detail = toiletDetail
        address.text = getCompleteAddress(detail)
        openingTimes.text = "${getString(R.string.times)} ${detail.fields.horaires_ouverture}"
        administrator.text = detail.fields.gestionnaire
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    fun shareAddress(view: View) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        val text = "${getString(R.string.toilets_at)} ${getCompleteAddress(detail)}: http://maps.google.com/?q=${detail.fields.geom_x_y[0]},${detail.fields.geom_x_y[1]}"
        sendIntent.putExtra(Intent.EXTRA_TEXT, text)
        sendIntent.type = "text/plain"
        startActivity(Intent.createChooser(sendIntent, resources.getText(R.string.send_to)))
    }

    fun openMap(view: View) {
        val intent = Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("geo:0,0?q=${detail.fields.geom_x_y[0]},${detail.fields.geom_x_y[1]}"))
        startActivity(intent)
    }

}