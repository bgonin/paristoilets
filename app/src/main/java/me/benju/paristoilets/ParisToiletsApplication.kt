package me.benju.paristoilets

import android.app.Application
import com.orhanobut.hawk.Hawk

class ParisToiletsApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        // Init what you need
        Hawk.init(this).build()
    }
}