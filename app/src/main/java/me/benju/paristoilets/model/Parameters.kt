package me.benju.paristoilets.model

data class Parameters(
        val dataset: List<String>,
        val timezone: String,
        val rows: Int,
        val format: String
)