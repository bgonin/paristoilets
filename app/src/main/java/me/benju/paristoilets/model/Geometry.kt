package me.benju.paristoilets.model

import android.os.Parcel
import android.os.Parcelable

data class Geometry(
        val type: String,
        val coordinates: List<Double>
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            ArrayList<Double>().apply { source.readList(this, Double::class.java.classLoader) }
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(type)
        writeList(coordinates)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Geometry> = object : Parcelable.Creator<Geometry> {
            override fun createFromParcel(source: Parcel): Geometry = Geometry(source)
            override fun newArray(size: Int): Array<Geometry?> = arrayOfNulls(size)
        }
    }
}