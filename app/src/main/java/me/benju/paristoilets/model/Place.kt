package me.benju.paristoilets.model

import android.os.Parcel
import android.os.Parcelable

data class Place(
        val recordid: String,
        val datasetid: String,
        val geometry: Geometry,
        val fields: Fields,
        val record_timestamp: String,
        var distance: Double = 0.0
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readParcelable<Geometry>(Geometry::class.java.classLoader),
            source.readParcelable<Fields>(Fields::class.java.classLoader),
            source.readString(),
            source.readDouble()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(recordid)
        writeString(datasetid)
        writeParcelable(geometry, 0)
        writeParcelable(fields, 0)
        writeString(record_timestamp)
        writeDouble(distance)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Place> = object : Parcelable.Creator<Place> {
            override fun createFromParcel(source: Parcel): Place = Place(source)
            override fun newArray(size: Int): Array<Place?> = arrayOfNulls(size)
        }
    }
}