package me.benju.paristoilets.model

data class Records(
        val nhits: Int,
        val parameters: Parameters,
        val records: List<Place>
)