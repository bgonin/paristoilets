package me.benju.paristoilets.model

import android.os.Parcel
import android.os.Parcelable

//TODO @Parcelize
data class Fields(
        val objectid: Int,
        val source: String,
        val arrondissement: String,
        val nom_voie: String,
        val gestionnaire: String,
        val geom_x_y: List<Double>,
        val x: Double,
        val y: Double,
        val numero_voie: String? = "",
        val identifiant: String,
        val horaires_ouverture: String? = ""
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            ArrayList<Double>().apply { source.readList(this, Double::class.java.classLoader) },
            source.readDouble(),
            source.readDouble(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(objectid)
        writeString(source)
        writeString(arrondissement)
        writeString(nom_voie)
        writeString(gestionnaire)
        writeList(geom_x_y)
        writeDouble(x)
        writeDouble(y)
        writeString(numero_voie)
        writeString(identifiant)
        writeString(horaires_ouverture)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Fields> = object : Parcelable.Creator<Fields> {
            override fun createFromParcel(source: Parcel): Fields = Fields(source)
            override fun newArray(size: Int): Array<Fields?> = arrayOfNulls(size)
        }
    }
}